function toRet () {
  return {
    state: {
      item: {},
      items: [],
      error: null,
      timestamps: {},
      durations: {
        item: 5 * 60 * 1000,
        items: 5 * 60 * 1000
      }
    },
    mutations: {
      setItem (state, res) {
        state.item = res
        state.timestamps.item = new Date().getTime()
      },
      setItems (state, res) {
        state.items = res
        state.timestamps.items = new Date().getTime()
      },
      setTimestamp (state, res) {
        state.timestamps[res] = new Date().getTime()
      },
      pushItem (state, res) {
        state.items.push(res)
      },
      putItem (state, res) {
        let alreadyExists = false
        state.items.forEach(item => {
          if (parseInt(item.id) === parseInt(res.id)) {
            alreadyExists = true
          }
        })
        if (!alreadyExists) {
          state.items.push(res)
        }
      },
      setError (state, res) {
        state.error = res
      }
    }
  }
}
module.exports = toRet
