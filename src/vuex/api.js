/**
 * Created by gabrieledisimone on 05/07/17.
 */
import axios from 'axios'
// http://5.189.170.235
// const INTERNAL_BASE_URL = 'http://' + window.location.hostname + ':9090/api'
const INTERNAL_BASE_URL = 'http://' + window.location.hostname + ':9999/api'
const ACL_URL = INTERNAL_BASE_URL + '/user/detail' // 'http://5.189.170.235:3123/api/user/detail'
const INT_MONITORED_INTERFACES = INTERNAL_BASE_URL + '/measurements'

const API_V1_URL = '/rest'
const API_V2_URL = '/api/v2'
// const ASSET_CONFIG_URL = '/js/onms-assets/config.json'
const NODES_URL = API_V1_URL + '/nodes'
const EVENTS_URL = API_V1_URL + '/events'
const ALARMS_URL = API_V1_URL + '/alarms'
const ACKNOWLEDGMENTS_URL = API_V1_URL + '/events'
const NOTIFICATIONS_URL = API_V1_URL + '/notifications'
const OUTAGES_URL = API_V1_URL + '/outages'
const RESOURCES_URL = API_V1_URL + '/resources'
const MEASUREMENTS_URL = API_V1_URL + '/measurements'
const GROUPS_URL = API_V1_URL + '/groups'
const AVAILABILITIES_URL = API_V1_URL + '/availability'
const GEOLOCATIONS_URL = API_V2_URL + '/geolocation'
const REQUISITIONS_URL = API_V1_URL + '/requisitions'
const CATEGORIES_SLUG = '/categories'
const NODES_SLUG = '/nodes'

let pipeline = []
let count = 0
let maxCount = 9999

let scheduleRequest = function () {
  // console.log('1.c pipeline')
  // console.log(pipeline)
  if (count < maxCount) {
    let value = pipeline.shift()
    if (value) {
      // console.log('1.d value')
      // console.log(value)
      // console.log('1.e new pipeline')
      // console.log(pipeline)
      doRequest(value)
    }
  }
}

let doRequest = function ({method, url, params, data, headers, resolve, reject}) {
  // console.log('2.a count')
  // console.log(count)
  let fullpath = ''
  if (url.includes(API_V1_URL) || url.includes(API_V2_URL)) {
    fullpath = window.localStorage.getItem('address') + url
  } else {
    fullpath = url
  }
  count = count + 1
  // console.log('2.b count incremented')
  // console.log(count)
  if (typeof headers === 'undefined') {
    headers = {}
  }

  headers['Accept'] = 'application/json'
  axios({
    method: method,
    url: fullpath,
    params: params,
    data: data,
    auth: {
      username: window.localStorage.getItem('username'),
      password: window.localStorage.getItem('password')
    },
    headers: headers
  })
    .then(function (result) {
      // console.log('3.a count')
      // console.log(count)
      count = count - 1
      // console.log('3.b count decremented')
      // console.log(count)
      scheduleRequest()
      resolve(result.data)
    })
    .catch(function (error) {
      // console.log('3.a - err - count')
      // console.log(count)
      count = count - 1
      // console.log('3.b - err - count decremented')
      // console.log(count)
      scheduleRequest()
      reject(error)
    })
}

let request = function (method, url, params, data, headers) {
  return new Promise(function (resolve, reject) {
    window.setTimeout((resolve, reject) => {
      // console.log('1.a pipeline - do')
      // console.log(pipeline)
      pipeline.push({
        method: method,
        url: url,
        params: params,
        data: data,
        headers: headers,
        resolve: resolve,
        reject: reject
      })
      // console.log('1.b new pipeline - do')
      // console.log(pipeline)
      window.setTimeout(() => {
        scheduleRequest()
      }, 0)
    }, 0.1 * count, resolve, reject)
  })
}

let getRequest = function (url, params) {
  return request('GET', url, params)
}

let postRequest = function (url, data, headers) {
  return request('POST', url, null, data, headers)
}

let api = {
  acl: {
    url: ACL_URL,
    methods: {
      getAcl (username, address) {
        let url = api.acl.url
        return postRequest(url, {obj: {username, address}})
      }
    }
  },
  nodes: {
    url: NODES_URL,
    methods: {
      getNodes (limit = 10) {
        let url = api.nodes.url
        return getRequest(url, {limit: limit})
      },
      getNodeDetail (id) {
        let url = api.nodes.url
        return getRequest(url + '/' + id)
      },
      getNodeIPInterfaces (id) {
        let url = api.nodes.url
        return getRequest(url + '/' + id + '/' + 'ipinterfaces')
      },
      getNodeSNMPInterfaces (id) {
        let url = api.nodes.url
        return getRequest(url + '/' + id + '/' + 'snmpinterfaces')
      },
      getNodeAsset (id) {
        let url = api.nodes.url
        return getRequest(url + '/' + id + '/' + 'assetRecord')
      },
      getNodeInfo (id) {
        let url = INTERNAL_BASE_URL + '/nodes/infocst'
        return getRequest(url + '/' + id)
      }
    }
  },
  events: {
    url: EVENTS_URL,
    methods: {
      getEvents (limit) {
        let url = api.events.url
        let params = {
          limit: 1000,
          orderBy: 'id',
          order: 'asc'
        }
        /* if (typeof limit !== 'undefined') {
          params['limit'] = limit
        } */
        return getRequest(url, params)
      },
      getEventsOfNode (nodeLabel) {
        let url = api.events.url
        let params = {
          // query: 'nodeLabel = \'' + nodeLabel + '\'',
          'node.label': nodeLabel,
          limit: 50,
          orderBy: 'id',
          order: 'asc'
        }
        return getRequest(url, params)
      }
    }
  },
  alarms: {
    url: ALARMS_URL,
    methods: {
      getAlarms () {
        let url = api.alarms.url
        return getRequest(url)
      }
    }
  },
  geolocations: {
    url: GEOLOCATIONS_URL,
    methods: {
      getGeolocations () {
        let url = api.geolocations.url
        let headers = {}
        headers['Content-Type'] = 'application/json'
        return postRequest(url, { strategy: 'Alarms', severityFilter: 'Normal', includeAcknowledgedAlarms: false }, headers)
      }
    }
  },
  acknowledgements: {
    url: ACKNOWLEDGMENTS_URL,
    methods: {
      getAcknowledgements () {
        let url = api.acknowledgements.url
        return getRequest(url)
      }
    }
  },
  availabilities: {
    url: AVAILABILITIES_URL,
    methods: {
      getAvailabilities () {
        let url = api.availabilities.url
        return getRequest(url)
      },
      getAvailabilitiesForNodes (category) {
        let url = api.availabilities.url + CATEGORIES_SLUG + '/' + category + '/' + NODES_SLUG
        return getRequest(url)
      }
    }
  },
  notifications: {
    url: NOTIFICATIONS_URL,
    methods: {
      getNotifications () {
        let url = api.notifications.url
        return getRequest(url)
      }
    }
  },
  outages: {
    url: OUTAGES_URL,
    methods: {
      getOutages (type, offset) {
        let url = api.outages.url
        let params = {
          offset: 0,
          limit: 5000
        }
        if (typeof type !== 'undefined') {
          params['ifRegainedService'] = type
        }
        if (typeof offset !== 'undefined') {
          params['offset'] = offset
        }
        return getRequest(url, params)
      }
    }
  },
  resources: {
    url: RESOURCES_URL,
    methods: {
      getResources () {
        let url = api.resources.url
        return getRequest(url)
      },
      getResourcesForNode (nodeId) {
        let url = api.resources.url + '/fornode/' + nodeId
        return getRequest(url)
      }
    }
  },
  measurements: {
    url: MEASUREMENTS_URL,
    methods: {
      getResourceMeasurementsForAttribute (resource, attribute, params) {
        let url = api.measurements.url + '/' + encodeURIComponent(resource) + '/' + attribute
        return getRequest(url, params)
      }
    }
  },
  requisitions: {
    url: REQUISITIONS_URL,
    methods: {
      getRequisitions () {
        let url = api.requisitions.url
        return getRequest(url)
      }
    }
  },
  monitoredInterfaces: {
    url: INT_MONITORED_INTERFACES,
    methods: {
      getMonitoredInterfaces (username, address) {
        let url = api.monitoredInterfaces.url
        return getRequest(url, {username: username, address: address})
      },
      getRealTimeMonitoredInterfaces (username, address, password, nodeId, hourFilter) {
        let url = api.monitoredInterfaces.url + '/realtime'
        return getRequest(url, {username: username, address: address, password: password, nodeId: nodeId, hourFilter: hourFilter})
      },
      getCassandraInterfaces (username, address, password, nodeId, from, to) {
        let url = api.monitoredInterfaces.url + '/cassandra'
        return getRequest(url, {username: username, address: address, password: password, nodeId: nodeId, from: from, to: to})
      }
    }
  },
  groups: {
    url: GROUPS_URL,
    methods: {
      getGroups () {
        let url = api.groups.url
        return getRequest(url)
      },
      getCategories (groupname) {
        let url = api.groups.url + '/' + groupname + CATEGORIES_SLUG
        return getRequest(url)
      }
    }
  },
  doRequest (context, res, cb) {
    return new Promise((resolve, reject) => {
      if (((context.state.timestamps[res] || 0) + (context.state.durations[res] || 0)) < (new Date().getTime())) {
        cb(resolve, reject)
      } else {
        resolve(context.state[res])
      }
    })
  }
}

export default api
