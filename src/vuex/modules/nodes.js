let items = new (require('../classes/items'))()
import Vue from 'vue'
import api from '../api'
let stateVuex = items.state
stateVuex['stateIpInterfaces'] = {}
stateVuex['snmpInterfaces'] = []
stateVuex['durations'].item = 0
stateVuex['durations'].stateIpInterfaces = 5 * 60 * 1000
stateVuex['durations'].snmpInterfaces = 5 * 60 * 1000
let mutationsVuex = items.mutations
mutationsVuex['setIPInterfaces'] = function (state, res) {
  let key = res[0].nodeId
  Vue.set(state.stateIpInterfaces, 'node' + key, res)
  state.timestamps.stateIpInterfaces = new Date().getTime()
}
mutationsVuex['setSNMPInterfaces'] = function (state, res) {
  state.snmpInterfaces = res
  state.timestamps.snmpInterfaces = new Date().getTime()
}
mutationsVuex['setNodeInfoCST'] = function (state, res) {
  state.nodeInfoCST = res
}

export default {
  namespaced: true,
  state: stateVuex,
  mutations: mutationsVuex,
  actions: {
    getNodes (context, limit = 9999) {
      return api.doRequest(context, 'items', (resolve, reject) => {
        api
          .nodes
          .methods
          .getNodes(limit)
          .then(function (result) {
            let acl = context.getters['getAcl']
            let nodesToInclude = acl.nodes
            let toRet = result.node.filter(node => nodesToInclude.indexOf(parseInt(node.id)) !== -1)
            if (acl.showAllNodes) {
              toRet = result.node
            }
            context.commit('setItems', toRet)
            resolve(toRet)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    },
    getNodeDetail (context, row) {
      if (row.label) {
        let cst = row.label.split(' - ')[0]
        return api.doRequest(context, 'item', (resolve, reject) => {
          api
            .nodes
            .methods
            .getNodeInfo(cst)
            .then(function (info) {
              api
                .nodes
                .methods
                .getNodeDetail(row.id)
                .then(function (result) {
                  result.infoCST = info
                  console.log(result)
                  context.commit('setItem', result)
                  resolve(result)
                })
                .catch(function (error) {
                  context.commit('setError', error)
                  reject(error)
                })
            })
            .catch(function (error) {
              context.commit('setError', error)
              reject(error)
            })
        })
      } else {
        return api.doRequest(context, 'item', (resolve, reject) => {
          api
            .nodes
            .methods
            .getNodeDetail(row.id)
            .then(function (result) {
              result.infoCST = {}
              context.commit('setItem', result)
              resolve(result)
            })
            .catch(function (error) {
              context.commit('setError', error)
              reject(error)
            })
        })
      }
    },
    getMonitoredNodeDetail (context, id) {
      return api.doRequest(context, 'item', (resolve, reject) => {
        api
          .nodes
          .methods
          .getNodeDetail(id)
          .then(function (result) {
            context.commit('putItem', result)
            resolve(result)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    },
    getNodeIPInterfaces (context, id) {
      return api.doRequest(context, 'stateIpInterfaces', (resolve, reject) => {
        api
          .nodes
          .methods
          .getNodeIPInterfaces(id)
          .then(function (result) {
            context.commit('setIPInterfaces', result.ipInterface)
            resolve(result.ipInterface)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    },
    getNodeSNMPInterfaces (context, id) {
      return api.doRequest(context, 'snmpInterfaces', (resolve, reject) => {
        api
          .nodes
          .methods
          .getNodeSNMPInterfaces(id)
          .then(function (result) {
            context.commit('setSNMPInterfaces', result.snmpInterface)
            resolve(result.snmpInterface)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    }
  },
  getters: {
    node (state) {
      return state.item
    },
    nodeById (state, getters) {
      return function (id) {
        return getters['nodes']().filter(node => parseInt(node.id) === parseInt(id))[0] || {}
      }
    },
    nodes (state) {
      return function (params) {
        let tot = state.items
        let toRet = []
        if (typeof params !== 'undefined') {
          tot.forEach(function (entry) {
            let obj = {}
            params.forEach(function (key) {
              obj[key] = entry[key]
            })
            toRet.push(obj)
          })
        } else {
          toRet = tot
        }
        return toRet
      }
    },
    nodeCategories (state) {
      let groups = window.localStorage.getItem('groups')
      let toRet = []
      let toRetObj = {}
      let categories = {}
      let tot = JSON.parse(JSON.stringify(state.items))

      tot.forEach(function (node) {
        let newCategories = []
        let newCategoriesObj = {}
        node.categories.forEach(function (category) {
          category.authorizedGroups.forEach(function (authorizedGroup) {
            if (groups.indexOf(authorizedGroup) !== -1) {
              newCategoriesObj[authorizedGroup] = category
            }
          })
        })
        Object.keys(newCategoriesObj).forEach(function (key) {
          newCategories.push(newCategoriesObj[key])
        })
        node.categories = newCategories
      })
      tot.forEach(function (node) {
        node.categories.forEach(function (category) {
          categories[category.id] = category.name
          if (typeof toRetObj[category.id] === 'undefined') {
            toRetObj[category.id] = []
          }
          toRetObj[category.id].push(node)
        })
      })
      Object.keys(toRetObj).forEach(function (key) {
        toRet.push({
          id: key,
          name: categories[key],
          nodes: toRetObj[key],
          nodesLength: toRetObj[key].length
        })
      })
      return toRet
    },
    getIpInterfaces (state) {
      return (id) => state.stateIpInterfaces['node' + id]
    },
    getMonitoredIpInterfacesOfCategory (state, getters) {
      return function (categoryId) {
        let monitoredInterfaces = []
        let nodes = getters['nodesIdOfCategory'](categoryId)
        nodes.forEach(node => {
          let nodeInterfaces = getters['getIpInterfaces'](node.nodeId) || []
          monitoredInterfaces.push(nodeInterfaces.filter(nodeInterface => {
            return nodeInterface.monitoredServiceCount > 0
          }))
        })
        return monitoredInterfaces
      }
    },
    getMonitoredIpInterfacesOfCategoryCount (state, getters) {
      return function (categoryId) {
        let count = 0
        let nodeIds = getters['nodesIdOfCategory'](categoryId)
        nodeIds.forEach(nodeId => {
          let nodeInterfaces = getters['getIpInterfaces'](nodeId) || []
          nodeInterfaces.forEach(nodeInterface => {
            count += nodeInterface.monitoredServiceCount
          })
        })
        return count
      }
    },
    snmpInterfaces (state) {
      return state.snmpInterfaces
    },
    nodesIdOfCategory (state) {
      return function (categoryId) {
        let nodesId = []
        let nodes = state.items.filter(node => {
          return node.categories.filter(category => {
            return parseInt(category.id) === parseInt(categoryId)
          }).length > 0
        })
        nodes.forEach(node => {
          nodesId.push(parseInt(node.id))
        })
        return nodesId
      }
    },
    getAcl (state, getters, rootState, rootGetters) {
      return rootGetters['users/acl']
    }
  }
}
