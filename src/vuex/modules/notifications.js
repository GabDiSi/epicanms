let items = new (require('../classes/items'))()
import api from '../api'

let state = items.state
let mutations = items.mutations

export default {
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: {
    getNotifications (context) {
      return api
        .notifications
        .methods
        .getNotifications()
        .then(function (result) {
          context.commit('setItems', result)
        })
        .catch(function (error) {
          context.commit('setError', error)
        })
    }
  },
  getters: {
    notifications () {
      return state.items
    }
  }
}
