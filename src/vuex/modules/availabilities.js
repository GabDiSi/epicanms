import api from '../api'
let items = new (require('../classes/items'))()
let stateVuex = items.state
stateVuex['nodeAvailabilities'] = []
stateVuex['durations'].nodeAvailabilities = 0
let mutationsVuex = items.mutations
mutationsVuex['setNodeAvailabilities'] = function (state, res) {
  state.nodeAvailabilities = res
}

export default {
  namespaced: true,
  state: stateVuex,
  mutations: mutationsVuex,
  actions: {
    getAvailabilities (context) {
      return api.doRequest(context, 'items', (resolve, reject) => {
        api
          .availabilities
          .methods
          .getAvailabilities()
          .then(function (result) {
            console.log(result)
            let toRet = result.section
            context.commit('setItems', toRet)
            resolve(toRet)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    },
    getAvailabilitiesForNodes (context, category) {
      return api.doRequest(context, 'nodeAvailabilities', (resolve, reject) => {
        api
          .availabilities
          .methods
          .getAvailabilitiesForNodes(category)
          .then(function (result) {
            console.log('normal')
            console.log(result)
            let toRet = result.node
            context.commit('setNodeAvailabilities', toRet)
            resolve(toRet)
          })
          .catch(function (error) {
            console.log('error')
            console.log(error)
            context.commit('setError', error)
            reject(error)
          })
      })
    }
  },
  getters: {
    availabilities (state) {
      return state.items
    },
    nodeAvailabilities (state) {
      return state.nodeAvailabilities
    }
  }
}
