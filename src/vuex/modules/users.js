import api from '../api'
let items = new (require('../classes/items'))()
let stateVuex = items.state
let mutationsVuex = items.mutations
stateVuex.item = {
  logged: false,
  acl: {
    nodes: []
  }
}
mutationsVuex['backgroundLogin'] = function (state) {
  try {
    state.item = {
      logged: window.localStorage.getItem('logged'),
      address: window.localStorage.getItem('address'),
      username: window.localStorage.getItem('username'),
      password: window.localStorage.getItem('password'),
      acl: JSON.parse(window.localStorage.getItem('acl'))
    }
  } catch (e) {
    window.localStorage.clear()
    window.location.reload()
  }
}

export default {
  namespaced: true,
  state: stateVuex,
  mutations: mutationsVuex,
  actions: {
    getNodes (context, {limit = 999, acl = {nodes: [], departments: [], divisions: []}}) {
      return api.doRequest(context, 'items', (resolve, reject) => {
        api
          .nodes
          .methods
          .getNodes(limit)
          .then((result) => {
            let nodes = []
            result.node.forEach(node => {
              let isInCategory = node.categories.filter((category) => (acl.categories.indexOf(category.name) !== -1)).length > 0
              if (acl.departments.indexOf(node.department) !== -1 || acl.divisions.indexOf(node.division) !== -1 || isInCategory) {
                nodes.push(parseInt(node.id))
              }
            })
            resolve(nodes)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    },
    setCredentials (context, credentials) {
      credentials.acl = {nodes: []}
      window.localStorage.setItem('address', credentials.address)
      window.localStorage.setItem('username', credentials.username)
      window.localStorage.setItem('password', credentials.password)
      window.localStorage.setItem('acl', credentials.acl)
      context.commit('setItem', credentials)
    },
    login (context) {
      return new Promise((resolve, reject) => {
        api
          .acl
          .methods
          .getAcl(window.localStorage.getItem('username'), window.localStorage.getItem('address'))
          .then(result => {
            if (result && result.response && result.response.acl) {
              let response = result.response.acl
              context.dispatch('getNodes', {limit: 9999, acl: response})
                .then((nodes) => {
                  nodes.forEach((node) => {
                    if (response.nodes.indexOf(node) === -1) {
                      response.nodes.push(node)
                    }
                  })
                  window.localStorage.setItem('logged', true)
                  window.localStorage.setItem('acl', JSON.stringify(response))
                  context.commit('backgroundLogin')
                  resolve()
                })
                .catch(function (error) {
                  reject(error)
                })
            } else {
              context.commit('setError', true)
              reject(true)
            }
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    },
    logout (context) {
      window.localStorage.clear()
      context.commit('setItem', {})
      window.location.reload()
    }
  },
  getters: {
    me (state) {
      return state.item
    },
    acl (state) {
      console.log('acl', state.item.acl)
      return state.item.acl || {nodes: [], monitoredNodes: []}
    },
    pageStatus (state) {
      return (link, ruleNeeded = 0) => {
        return (
          state.item.acl &&
          state.item.acl.pages &&
          state.item.acl.pages.filter(
            pageRule => pageRule.name === link && pageRule.rule > ruleNeeded
          ).length > 0
        )
      }
    },
    widgetRuleStatus (state) {
      return (name, ruleNeeded = 0) => {
        return (
          state.item.acl &&
          state.item.acl.widgets &&
          state.item.acl.widgets.filter(
            widgetRule => widgetRule.name === name && widgetRule.rule > ruleNeeded
          ).length > 0
        )
      }
    },
    widgetProperties (state) {
      return (name) => {
        let toRet = []
        if (state.item.acl && state.item.acl.widgets) {
          state.item.acl.widgets.filter(widgetRule => widgetRule.name === name).forEach((widgetRule) => {
            toRet = widgetRule.properties
          })
        }
        return toRet
      }
    },
    widgetPropertyStatus (state) {
      return (name, property) => {
        return (
          state.item.acl &&
          state.item.acl.widgets &&
          state.item.acl.widgets.filter(
            widgetRule => (
              widgetRule.name === name &&
              widgetRule.properties.filter(
                itemRule => itemRule.properties.indexOf(property) !== -1
              ).length > 0
            )
          ).length > 0
        )
      }
    },
    assetWidgetRuleStatus (state) {
      return (name, ruleNeeded = 0) => {
        return (
          state.item.acl &&
          state.item.acl.assetWidgets &&
          state.item.acl.assetWidgets.filter(
            widgetRule => widgetRule.name === name && widgetRule.rule > ruleNeeded
          ).length > 0
        )
      }
    },
    assetWidgetProperties (state) {
      return (name) => {
        let toRet = []
        if (state.item.acl && state.item.acl.assetWidgets) {
          state.item.acl.assetWidgets.filter(widgetRule => widgetRule.name === name).forEach((widgetRule) => {
            toRet = widgetRule.properties
          })
        }
        return toRet
      }
    },
    assetWidgetPropertyStatus (state) {
      return (name, property) => {
        return (
          state.item.acl &&
          state.item.acl.assetWidgets &&
          state.item.acl.assetWidgets.filter(
            widgetRule => (
              widgetRule.name === name &&
              widgetRule.properties.filter(
                itemRule => itemRule.properties.indexOf(property) !== -1
              ).length > 0
            )
          ).length > 0
        )
      }
    },
    graphResourceRuleStatus (state) {
      return (nodeName, name, ruleNeeded = 0) => {
        if (state.item.acl && state.item.acl.graphResourcesForNode) {
          let graphNode = null
          state.item.acl.graphResourcesForNode.forEach(res => {
            if (parseInt(res.name) === parseInt(nodeName)) {
              graphNode = res
            }
          })
          if (graphNode) {
            console.log('graphNode', graphNode)
            let toRet = graphNode.properties.filter(
                graphType => graphType.name === name && graphType.rule > ruleNeeded
              ).length > 0
            return toRet
          }
        }
        return 1
      }
    },
    graphResourceProperties (state) {
      return (nodeName, name) => {
        let toRet = []
        if (state.item.acl && state.item.acl.graphResourcesForNode) {
          let graphNode = null
          state.item.acl.graphResourcesForNode.forEach(res => {
            if (parseInt(res.name) === parseInt(nodeName)) {
              graphNode = res
            }
          })
          if (graphNode) {
            graphNode.properties.filter(graphRule => graphRule.name === name).forEach((graphRule) => {
              graphRule.forEach(graphProp => {
                if (graphProp.rule >= 1) {
                  toRet.push(graphRule.properties)
                }
              })
            })
          }
        }
        return toRet
      }
    },
    graphResourcesForNodeIsDefault (state) {
      return (nodeId) => {
        let toRet = false
        if (state.item.acl && state.item.acl.graphResourcesForNode) {
          let rules = state.item.acl.graphResourcesForNode.filter(rule => parseInt(rule.name) === parseInt(nodeId))
          toRet = rules && rules.length === 1 && rules[0].properties && rules[0].properties.length === 1 && rules[0].properties[0].name === 'loading...'
        }
        return toRet
      }
    },
    graphResourcePropertyStatus (state) {
      return (nodeName, name, property) => {
        if (state.item.acl && state.item.acl.graphResourcesForNode) {
          let graphNode = null
          state.item.acl.graphResourcesForNode.forEach(res => {
            if (parseInt(res.name) === parseInt(nodeName)) {
              graphNode = res
            }
          })
          if (graphNode) {
            let graphProp = null
            graphNode.properties.filter(graphRule => graphRule.name === name).forEach((graphRule) => {
              graphProp = graphRule.properties.filter(prop => prop.name === property)
            })
            if (graphProp && graphProp.length > 0) {
              return graphProp[0].rule
            }
          }
        }
        return 1
      }
    },
    isLogged (state) {
      return state.item.logged
    },
    isBackgroundLogged () {
      return window.localStorage.getItem('logged')
    }
  }
}
