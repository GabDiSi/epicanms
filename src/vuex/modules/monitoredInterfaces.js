let items = new (require('../classes/items'))()
import api from '../api'

let state = items.state
let mutations = items.mutations
state['realTimeItems'] = []
mutations['setRealTimeItems'] = function (state, res) {
  state.realTimeItems = res
}
state['cassandraItems'] = []
mutations['setCassandraItems'] = function (state, res) {
  state.cassandraItems = res
}

state.durations.cassandraItems = 0

export default {
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: {
    getMonitoredInterfaces (context) {
      return api
        .monitoredInterfaces
        .methods
        .getMonitoredInterfaces(window.localStorage.getItem('username'), window.localStorage.getItem('address'))
        .then(function (result) {
          context.commit('setItems', result.response[0].interfaces)
        })
        .catch(function (error) {
          context.commit('setError', error)
        })
    },
    getRealTimeMonitoredInterfaces (context, params) {
      console.log(params)
      return api
        .monitoredInterfaces
        .methods
        .getRealTimeMonitoredInterfaces(window.localStorage.getItem('username'), window.localStorage.getItem('address'), window.localStorage.getItem('password'), params.nodeId, params.hourFilter)
        .then(function (result) {
          console.log('result', result)
          context.commit('setRealTimeItems', result.response)
        })
        .catch(function (error) {
          context.commit('setError', error)
        })
    },
    getCassandraInterfaces (context, params) {
      console.log('params', params)
      return api
        .monitoredInterfaces
        .methods
        .getCassandraInterfaces(window.localStorage.getItem('username'), window.localStorage.getItem('address'), window.localStorage.getItem('password'), params.nodeId, params.from, params.to)
        .then(function (result) {
          console.log('cassandra', result)
          context.commit('setCassandraItems', result.response)
        })
        .catch(function (error) {
          context.commit('setError', error)
        })
    },
    clearCassandraInterfaces (context, params) {
      context.commit('setCassandraItems', [])
    }
  },
  getters: {
    monitoredInterfaces () {
      return state.items
    },
    realTimeMonitoredInterfaces () {
      return state.realTimeItems
    },
    cassandraInterfaces () {
      return state.cassandraItems
    }
  }
}
