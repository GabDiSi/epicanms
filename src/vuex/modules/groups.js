import api from '../api'

let items = new (require('../classes/items'))()
let stateVuex = items.state
stateVuex['categories'] = []
let mutationsVuex = items.mutations
mutationsVuex['pushCategory'] = function (state, res) {
  if (state.categories.every((item) => {
    return item.id !== res.id
  })) {
    state.categories.push(res)
  }
  window.localStorage.setItem('categories', JSON.stringify(state.categories))
}

export default {
  namespaced: true,
  state: stateVuex,
  mutations: mutationsVuex,
  actions: {
    getGroups (context) {
      let username = window.localStorage.getItem('username')
      let promise = api
        .groups
        .methods
        .getGroups()

      return new Promise(function (resolve, reject) {
        promise
          .then(function (result) {
            let groups = []
            result.group.forEach((group) => {
              if (group.user.indexOf(username) !== -1) {
                groups.push(group.name)
              }
            })
            context.commit('setItems', groups)
            window.localStorage.setItem('groups', JSON.stringify(groups))
            resolve(groups)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject()
          })
      })
    },
    getCategories (context, groupname) {
      let promise = api
        .groups
        .methods
        .getCategories(groupname)

      return new Promise(function (resolve, reject) {
        promise
          .then(function (result) {
            console.log(result)
            result.category.forEach((category) => {
              context.commit('pushCategory', category)
            })
            resolve()
          })
          .catch(function (error) {
            console.log(error)
            context.commit('setError', error)
            reject(error)
          })
      })
    }
  },
  getters: {
    groups (state) {
      return state.items
    },
    categories (state) {
      return state.categories
    }
  }
}
