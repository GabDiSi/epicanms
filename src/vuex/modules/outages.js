import api from '../api'
import moment from 'moment'
let items = new (require('../classes/items'))()
let stateVuex = items.state
let mutationsVuex = items.mutations

export default {
  namespaced: true,
  state: stateVuex,
  mutations: mutationsVuex,
  actions: {
    getOutages (context) {
      return api.doRequest(context, 'items', (resolve, reject) => {
        api
          .outages
          .methods
          .getOutages()
          .then(function (result) {
            let acl = context.getters['getAcl']
            let nodesToInclude = acl.nodes
            let toRet = result.outage.filter(outage => nodesToInclude.indexOf(parseInt(outage.nodeId)) !== -1)
            if (acl.showAllNodes) {
              toRet = result.outage
            }
            context.commit('setItems', toRet)
            resolve(toRet)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    }
  },
  getters: {
    outages (state) {
      return state.items
    },
    outagesOfNodes (state) {
      return function (nodesId) {
        return state.items.filter(item => {
          return nodesId.indexOf(parseInt(item.nodeId)) !== -1
        })
      }
    },
    interfacesWithOutagesOfNodes (state, getters) {
      return function (nodes, active, latest24H, ipAddress = true) {
        let interfaces = []
        let items = latest24H ? getters['latest24HOutages'] : getters['outages']
        items.forEach(item => {
          let toPush = ipAddress ? item.nodeId + ' - ' + item.ipAddress : item.nodeId
          if (((active && item.ifRegainedService === null) || !active) && nodes.indexOf(item.nodeId) !== -1 && interfaces.indexOf(toPush) === -1) {
            interfaces.push(toPush)
          }
        })
        return interfaces
      }
    },
    latest24HOutages (state) {
      return state.items.filter(item => {
        return parseInt(item.ifLostService) > +moment().add(-1, 'days') || parseInt(item.ifRegainedService) > +moment().add(-1, 'days') || item.ifRegainedService === null
      })
    },
    latest24HOutagesByNodes (state, getters) {
      return function (nodes) {
        let outages = {}
        nodes.forEach(nodeId => {
          outages[nodeId.toString()] = getters['latest24HOutages'].filter(item => {
            return nodeId === (parseInt(item.nodeId))
          })
        })
        return outages
      }
    },
    resolvedOutages (state) {
      return state.items.filter((item) => {
        return item.ifRegainedService
      })
    },
    currentOutages (state) {
      return state.items.filter((item) => {
        return !item.ifRegainedService
      })
    },
    getAcl (state, getters, rootState, rootGetters) {
      return rootGetters['users/acl']
    }
  }
}
