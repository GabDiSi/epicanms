let items = new (require('../classes/items'))()
import api from '../api'

let state = items.state
let mutations = items.mutations

export default {
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: {
    getRequisitions (context) {
      return api
        .requisitions
        .methods
        .getRequisitions()
        .then(function (result) {
          context.commit('setItems', result['model-import'])
        })
        .catch(function (error) {
          context.commit('setError', error)
        })
    }
  },
  getters: {
    requisitions () {
      return state.items
    }
  }
}
