import items from '../classes/items'
import api from '../api'

let state = items.state
let mutations = items.mutations

export default {
  state: state,
  mutations: mutations,
  actions: {
    getAcknowledgements (context) {
      api
        .acknowledgements
        .methods
        .getAcknowledgements()
        .then(function (result) {
          context.commit('setItems', result)
        })
        .catch(function (error) {
          context.commit('setError', error)
        })
    }
  },
  getters: {
    acknowledgements (state) {
      return state.items
    }
  }
}
