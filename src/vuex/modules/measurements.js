import api from '../api'
import Vue from 'vue'

let items = new (require('../classes/items'))()
let stateVuex = items.state
stateVuex['itemsByNode'] = {}
let mutationsVuex = items.mutations
mutationsVuex['setItemsByNode'] = function (state, res) {
  state.itemsByNode = res
}
mutationsVuex['putItem'] = function (state, res) {
  let key = res.nodeId + ''
  let resource = res.resource
  let attribute = res.attribute

  if (!state.itemsByNode[key]) {
    Vue.set(state.itemsByNode, key, {})
  }

  if (!state.itemsByNode[key][resource]) {
    Vue.set(state.itemsByNode[key], resource, {})
  }

  Vue.set(state.itemsByNode[key][resource], attribute, res)
}

export default {
  namespaced: true,
  state: stateVuex,
  mutations: mutationsVuex,
  actions: {
    getResourceMeasurementsForAttribute (context, params) {
      let promise = api
        .measurements
        .methods
        .getResourceMeasurementsForAttribute(params.resource, params.attribute, params.params)

      promise
        .then(function (result) {
          context.commit('pushItem', result)
        })
        .catch(function (error) {
          context.commit('setError', error)
        })
    },
    getResourceMeasurementsForDashboardAttribute (context, params) {
      let promise = api
        .measurements
        .methods
        .getResourceMeasurementsForAttribute(params.resource, params.attribute, params.params)

      promise
        .then(function (result) {
          result.nodeId = params.nodeId
          result.resource = params.resource
          result.attribute = params.attribute
          context.commit('putItem', result)
        })
        .catch(function (error) {
          context.commit('setError', error)
        })
    }
  },
  getters: {
    measurements (state) {
      return state.items
    },
    measurementsByNode (state) {
      return state.itemsByNode
    }
  }
}
