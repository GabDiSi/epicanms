import api from '../api'

let items = new (require('../classes/items'))()
let stateVuex = items.state
stateVuex['config'] = {}
let mutationsVuex = items.mutations
mutationsVuex['setConfig'] = function (state, res) {
  state.config = res
}

export default {
  namespaced: true,
  state: stateVuex,
  mutations: mutationsVuex,
  actions: {
    getAsset (context, nodeId) {
      let promise = api
        .nodes
        .methods
        .getNodeAsset(nodeId)

      promise
        .then(function (result) {
          context.commit('setItem', result)
        })
        .catch(function (error) {
          context.commit('setError', error)
        })
    } /*,
    getAssetConfig (context) {
      let promise = api
        .nodes
        .methods
        .getAssetConfig()

      promise
        .then(function (result) {
          context.commit('setConfig', result)
        })
        .catch(function (error) {
          context.commit('setError', error)
        })
    } */
  },
  getters: {
    asset (state) {
      return state.item
    },
    assetConfig (state) {
      return state.item
    }
  }
}
