import api from '../api'
let items = new (require('../classes/items'))()
let stateVuex = items.state
let mutationsVuex = items.mutations

export default {
  namespaced: true,
  state: stateVuex,
  mutations: mutationsVuex,
  actions: {
    getGeolocations (context) {
      return api.doRequest(context, 'items', (resolve, reject) => {
        api
          .geolocations
          .methods
          .getGeolocations()
          .then(function (result) {
            let acl = context.getters['getAcl']
            let nodesToInclude = acl.nodes
            let toRet = result.filter(node => nodesToInclude.indexOf(parseInt(node.nodeInfo.nodeId)) !== -1)
            context.commit('setItems', toRet)
            resolve(toRet)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    }
  },
  getters: {
    geolocations (state) {
      return state.items
    },
    getAcl (state, getters, rootState, rootGetters) {
      return rootGetters['users/acl']
    }
  }
}
