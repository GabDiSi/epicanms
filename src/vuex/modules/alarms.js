import api from '../api'
let items = new (require('../classes/items'))()
let stateVuex = items.state
let mutationsVuex = items.mutations

let SEVERITY = {
  INDETERMINATE: 0,
  CLEARED: 1,
  NORMAL: 2,
  WARNING: 3,
  MINOR: 4,
  MAJOR: 5,
  CRITICAL: 6
}

export default {
  namespaced: true,
  state: stateVuex,
  mutations: mutationsVuex,
  actions: {
    getAlarms (context) {
      return api.doRequest(context, 'items', (resolve, reject) => {
        api
          .alarms
          .methods
          .getAlarms()
          .then(function (result) {
            let acl = context.getters['getAcl']
            let nodesToInclude = acl.nodes
            let toRet = result.alarm.filter(alarm => nodesToInclude.indexOf(parseInt(alarm.nodeId)) !== -1)
            if (acl.showAllNodes) {
              toRet = result.alarm
            }
            context.commit('setItems', toRet)
            resolve(toRet)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    }
  },
  getters: {
    alarms (state) {
      return state.items
    },
    activeAlarms (state) {
      let toRet = []
      let objRet = {}
      state.items.forEach(alarm => {
        if (typeof objRet[alarm.nodeId] === 'undefined') {
          objRet[alarm.nodeId] = {
            nodeId: alarm.nodeId,
            nodeLabel: alarm.nodeLabel,
            count: 0,
            severity: alarm.severity,
            lastEventTime: alarm.lastEventTime
          }
        }
        objRet[alarm.nodeId].count++
        if (SEVERITY[alarm.severity] > SEVERITY[objRet[alarm.nodeId].severity]) {
          objRet[alarm.nodeId].severity = alarm.severity
        }
        if (alarm.lastEventTime < objRet[alarm.nodeId].lastEventTime) {
          objRet[alarm.nodeId].lastEventTime = alarm.lastEventTime
        }
      })
      Object.keys(objRet).forEach(function (key) {
        toRet.push(objRet[key])
      })
      return toRet
    },
    alarmsFilter (state, getters, rootState, rootGetters) {
      return function (filter) {
        let items = JSON.parse(JSON.stringify(state.items))
        Object.keys(filter).forEach((key) => {
          if (filter[key]) {
            items = items.filter((item) => {
              let check = false
              if (key === 'firstEventsAfter') {
                check = item.firstEventTime >= new Date(filter[key]).getTime()
              } else if (key === 'firstEventsBefore') {
                check = item.firstEventTime <= new Date(filter[key]).getTime()
              } else if (key === 'lastEventsAfter') {
                check = item.lastEventTime >= new Date(filter[key]).getTime()
              } else if (key === 'lastEventsBefore') {
                check = item.lastEventTime <= new Date(filter[key]).getTime()
              } else if (key === 'nodeIp') {
                check = rootGetters['nodes/getIpInterfaces'](item.nodeId).filter((ipObject) => ipObject.ipAddress.match(new RegExp(filter[key], 'i'))).length > 0
              } else if (item[key]) {
                check = item[key].toString().match(new RegExp(filter[key], 'i'))
              }
              return check
            })
          }
        })
        return items
      }
    },
    getAcl (state, getters, rootState, rootGetters) {
      return rootGetters['users/acl']
    }
  }
}
