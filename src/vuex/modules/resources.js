import api from '../api'

let items = new (require('../classes/items'))()
let stateVuex = items.state
stateVuex['durations'].items = 0
let mutationsVuex = items.mutations

export default {
  namespaced: true,
  state: stateVuex,
  mutations: mutationsVuex,
  actions: {
    getResources (context) {
      return api.doRequest(context, 'items', (resolve, reject) => {
        let promise = api
          .resources
          .methods
          .getResources()

        promise
          .then(function (result) {
            context.commit('setItems', result.resource)
            resolve(result.resource)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    },
    getResourcesForNode (context, nodeId) {
      return api.doRequest(context, 'items', (resolve, reject) => {
        let promise = api
          .resources
          .methods
          .getResourcesForNode(nodeId)

        promise
          .then(function (result) {
            context.commit('setItems', [result])
            resolve([result])
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    }
  },
  getters: {
    resources (state) {
      return state.items
    },
    nodeResources (state) {
      return function (nodeLabel) {
        let array = state.items.filter(function (item) {
          console.log('a', item.label)
          console.log('b', nodeLabel)
          return item.label === nodeLabel
        })
        console.log('array', array)
        let toRet = []
        let toRetObj = {}
        let resources = array[0].children.resource
        resources.forEach(function (resource) {
          if (typeof toRetObj[resource.typeLabel] === 'undefined') {
            toRetObj[resource.typeLabel] = []
          }
          toRetObj[resource.typeLabel].push(resource)
        })
        Object.keys(toRetObj).forEach(function (key) {
          toRet.push({
            key: key,
            values: toRetObj[key]
          })
        })
        console.log('toRet', toRet)
        return toRet
      }
    }
  }
}
