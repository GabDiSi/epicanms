import api from '../api'
let items = new (require('../classes/items'))()
let stateVuex = items.state
let mutationsVuex = items.mutations
mutationsVuex['pushUniqueItem'] = function (state, res) {
  if (state.items.filter(item => parseInt(item.id) === parseInt(res.id)).length === 0) {
    state.items.push(res)
  }
}

export default {
  namespaced: true,
  state: stateVuex,
  mutations: mutationsVuex,
  actions: {
    getEvents (context, limit) {
      return api.doRequest(context, 'items', (resolve, reject) => {
        api
          .events
          .methods
          .getEvents(limit)
          .then(function (result) {
            let acl = context.getters['getAcl']
            let nodesToInclude = acl.nodes
            let toRet = result.event.filter(event => nodesToInclude.indexOf(parseInt(event.nodeId)) !== -1)
            if (acl.showAllNodes) {
              toRet = result.event
            }
            context.commit('setItems', toRet)
            resolve(toRet)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    },
    getEventsOfNode (context, nodeLabel) {
      return api.doRequest(context, 'item' + nodeLabel, (resolve, reject) => {
        api
          .events
          .methods
          .getEventsOfNode(nodeLabel)
          .then(function (result) {
            let acl = context.getters['getAcl']
            let nodesToInclude = acl.nodes
            let toRet = result.event.filter(event => nodesToInclude.indexOf(parseInt(event.nodeId)) !== -1)
            if (acl.showAllNodes) {
              toRet = result.event
            }
            toRet.forEach(item => {
              context.commit('pushUniqueItem', item)
            })
            resolve(toRet)
          })
          .catch(function (error) {
            context.commit('setError', error)
            reject(error)
          })
      })
    }
  },
  getters: {
    events (state) {
      return state.items
    },
    eventsOfNode (state) {
      return function (nodeId) {
        return state.items.filter(event => {
          return parseInt(event.nodeId) === parseInt(nodeId)
        })
      }
    },
    eventsFilter (state, getters, rootState, rootGetters) {
      return function (filter) {
        console.log(filter)
        let items = JSON.parse(JSON.stringify(state.items))
        Object.keys(filter).forEach((key) => {
          if (filter[key]) {
            items = items.filter((item) => {
              let check = false
              if (key === 'eventsAfter' || key === 'relativeTime') {
                check = item.time >= new Date(filter[key]).getTime()
              } else if (key === 'eventsBefore') {
                check = item.time <= new Date(filter[key]).getTime()
              } else if (key === 'nodeIp') {
                check = rootGetters['nodes/getIpInterfaces'](item.nodeId).filter((ipObject) => ipObject.ipAddress.match(new RegExp(filter[key], 'i'))).length > 0
              } else if (item[key]) {
                check = item[key].toString().match(new RegExp(filter[key], 'i'))
              }
              return check
            })
          }
        })
        return items
      }
    },
    getAcl (state, getters, rootState, rootGetters) {
      return rootGetters['users/acl']
    }
  }
}
