// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './lib/css'
import './lib/script'
import './lib/global'

import Vue from 'vue'
import App from './App'
import router from './router'
import EventBus from './lib/eventBus.js'
import axios from 'axios'
import store from 'vuex/store'
import {ClientTable, ServerTable} from 'vue-tables-2'

Vue.prototype.$bus = EventBus
Vue.prototype.$http = axios

Vue.use(ClientTable, {}, false, require('../node_modules/vue-tables-2/compiled/template.js')('client'))
Vue.use(ServerTable, {}, false, require('../node_modules/vue-tables-2/compiled/template.js')('client'))

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {
    App
  }
})
