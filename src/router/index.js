import Vue from 'vue'
import Router from 'vue-router'
import store from '../vuex/store'

import Unauthorized from '../epica/pages/Unauthorized.vue'
import Login from '../epica/pages/Login.vue'
import Dashboard from '../epica/pages/Dashboard.vue'
import Node from '../epica/pages/Node.vue'
import NodeDashboard from '../epica/pages/NodeDashboard.vue'
import Nodes from '../epica/pages/Nodes.vue'
import Resources from '../epica/pages/Resources.vue'
import Asset from '../epica/pages/Asset.vue'
import Hardware from '../epica/pages/Hardware.vue'
import Events from '../epica/pages/Events.vue'
import Alarms from '../epica/pages/Alarms.vue'
import CbQOS from '../epica/pages/CbQOS.vue'
import Notifications from '../epica/pages/Notifications.vue'
import Outages from '../epica/pages/Outages.vue'
import GraphResults from '../epica/pages/GraphResults.vue'
import Cassandra from '../epica/pages/Cassandra.vue'
import RTC from '../epica/pages/RTC.vue'

Vue.use(Router)

let namespaces = {
  users: 'users/',
  groups: 'groups/'
}

let router = new Router({
  base: '/gui',
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
      props: true
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/nodes',
      name: 'Nodes',
      component: Nodes
    },
    {
      path: '/node/:id/:label',
      component: Node,
      beforeEnter: (to, from, next) => {
        let acl = store.getters[namespaces.users + 'acl']
        let id = to.params.id
        let accessibleNodes = acl.nodes
        if ((accessibleNodes.indexOf(parseInt(id)) === -1) && (!acl.showAllNodes)) {
          next({name: 'Unauthorized'})
        } else {
          next()
        }
      },
      children: [
        {
          path: '',
          name: 'Node.Dashboard',
          component: NodeDashboard
        },
        {
          path: 'alarms',
          name: 'Node.Alarms',
          component: Alarms,
          props: function (route) {
            return {
              query: route.query.filter
            }
          }
        },
        {
          path: 'asset',
          name: 'Node.Asset',
          component: Asset
        },
        {
          path: 'hardware',
          name: 'Node.Hardware',
          component: Hardware
        },
        {
          path: 'resources',
          name: 'Node.Resources',
          component: Resources,
          children: [
            {
              path: 'graphs',
              name: 'Node.Resources.GraphResults',
              component: GraphResults
            }
          ]
        },
        {
          path: 'cbqos',
          name: 'Node.CbQOS',
          component: Cassandra
        }
      ]
    },
    {
      path: '/events',
      name: 'Events',
      component: Events
    },
    {
      path: '/alarms',
      name: 'Alarms',
      component: Alarms
    },
    {
      path: '/cbqos',
      name: 'CbQOS',
      component: CbQOS
    },
    {
      path: '/notifications',
      name: 'Notifications',
      component: Notifications
    },
    {
      path: '/outages',
      name: 'Outages',
      component: Outages
    },
    {
      path: '/rtc/:category',
      name: 'RTC',
      component: RTC
    },
    {
      path: '/unauthorized',
      name: 'Unauthorized',
      component: Unauthorized
    },
    {
      path: '*',
      redirect: { name: 'Dashboard' }
    }
  ],
  linkActiveClass: 'active'
})

router.beforeEach((to, from, next) => {
  if (store.getters[namespaces.users + 'isLogged'] || store.getters[namespaces.users + 'isBackgroundLogged'] || to.name === 'Login') {
    store.commit(namespaces.users + 'backgroundLogin')
    if (to.name === 'Unauthorized' || to.name === 'Login' || to.name === 'Node.Asset' || store.getters[namespaces.users + 'pageStatus'](to.name)) {
      next()
    } else {
      next({name: 'Unauthorized'})
    }
  } else {
    next({name: 'Login'})
  }
})

export default router
